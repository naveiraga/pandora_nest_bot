import telebot
from bson.objectid import ObjectId
from pymongo import MongoClient
from telebot import types
from io import BytesIO
import tempfile
import undetected_chromedriver.v2 as uc
import atexit
import os

import States_for_show as States
from core_for_show import divide_et_impera, edit_input
import web_interface_for_show as wb
from img_check_for_show import get_presence_in_dbs_werdict, resiser

bot = telebot.TeleBot('insert your key here')

dictsample_for_channel_post1 = {
    "id": 0,
    "src": "",
    "photo": "",
    "width": 0,
    "height": 0,
    "users_who_like": [],
    "users_who_dislike": [],
    "like_count": 0,
    "dislike_count": 0
}

dictsample_for_channel_post2 = {
    "id": 0,
    "src": "",
    "photo": "",
    "width": 0,
    "height": 0,
    "keyboard_type": 0,
    "users_who_like": [],
    "users_who_fine": [],
    "users_who_disgust": [],
    "like_count": 0,
    "fine_count": 0,
    "disgusting_count": 0,
}

tempdict_sample = {
    "userid": '',
    "status": '',
    "message_id": '',
    "photo_id": '',
    "size": '',
    "linkdict": '',
    "src": '',
    "tags": ''
}
client = MongoClient('localhost')
db = client['']
users = db['']

State = States.States(bot)

# class for store and handle user data before store it in db
class User:
    def __init__(self, userid=None, message_id=None, db_id=None):
        self.userid = userid
        self.message_id = message_id
        self.db_id = db_id

    def notice(self):
        bot.send_message(self.userid, "redirecting, please wait")
        db.users.update_one({}, {"$set": {str(self.userid): State.state3}})



new_user = User()

options = uc.ChromeOptions()
options.headless = True
options.add_argument('--blink-settings=imagesEnabled=false')
options.add_argument('--no-first-run')
options.add_argument('--no-service-autorun')
options.add_argument('--block-new-web-contents')
browser = uc.Chrome(options=options, version_main=95)


# dowloads photo sent by the user,
# returns tuple: path(str), bytes
def bot_downloadimage(message):
    directory = tempfile.gettempdir()
    file = bot.get_file(message.photo[-1].file_id)
    downloaded = bot.download_file(file.file_path)
    src = r'{}\{}'.format(directory, message.photo[1].file_unique_id)
    return src, downloaded


# check is user photo in bot db
# returns verdict is picture in db or not (str)
def bot_is_picture_already_posted(src, downloaded_file):
    with open(src, 'wb') as new_file:
        new_file.write(downloaded_file)
    verd = get_presence_in_dbs_werdict(src)
    if verd != 'passed':
        if verd.isdigit():
            bot.reply_to(verd, 'seems like you already posted this one before')
            return 1
        else:
            return 'seems like you already posted this one before:' \
                   '\nhttps://t.me{}'.format(verd)
    else:
        return 'passed'


# performs query to SauseNao or yandex,
#  returns tuple: (info about links get(str), site-link dict)
def bot_call_to_reverse_searchers(src, ignore=False):
    imageboard1 = {x: None for x in wb.imgbrd_dict_sample.keys()}
    web = wb.WebSearchUrl(src, board_dict=imageboard1, browser=browser)
    web.get_links(notice=new_user.notice, ignore_nao=ignore)
    check = web.check_urls()
    return check, imageboard1


# func for update callback buttons info, viewed by user
def buttongen_for_dinamic_likes(emotion, count, callback):
    if count > 0:
        button = types.InlineKeyboardButton("{} {}".format(emotion, count), callback_data=callback)
        return button
    else:
        button = types.InlineKeyboardButton("{}".format(emotion), callback_data=callback)
        return button

# two keyboards with interactive like buttons
# returns Telebot.types.InlineKeyboardMarkup
def dinamic_like_keyboard_with_two_buttons(db_number):
    post = db.posts.find_one({"_id": ObjectId(db_number)})
    like = buttongen_for_dinamic_likes('💚', post["like_count"], 'like_{}'.format(db_number))
    dislike = buttongen_for_dinamic_likes('👎', post["dislike_count"], 'dislike_{}'.format(db_number))
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.row(like, dislike)
    return keyboard


def dinamic_like_keyboard_with_three_buttons(db_number):
    post = db.posts.find_one({"_id": ObjectId(db_number)})
    like = buttongen_for_dinamic_likes('💓', post["like_count"], 'like_{}'.format(db_number))
    dislike = buttongen_for_dinamic_likes('👌', post["fine_count"], 'fine_{}'.format(db_number))
    disgusting = buttongen_for_dinamic_likes('😰', post["disgusting_count"], 'disgusting_{}'.format(db_number))
    keyboard = types.InlineKeyboardMarkup(row_width=3)
    keyboard.row(like, dislike, disgusting)
    return keyboard


# increase or decrease like count in bd, update buttons
def like_counter(callback_query: types.CallbackQuery, likelist, count):
    like_id = callback_query.data.split('_')[1]
    post = db.posts.find_one({"_id": ObjectId(str(like_id))})
    if callback_query.from_user.id in post[likelist]:
        db.posts.update_one({"_id": ObjectId(str(like_id))}, {'$pull': {likelist: callback_query.from_user.id}})
        db.posts.update_one({"_id": ObjectId(str(like_id))}, {'$inc': {count: -1}})
    else:
        db.posts.update_one({"_id": ObjectId(str(like_id))}, {'$push': {likelist: callback_query.from_user.id}})
        db.posts.update_one({"_id": ObjectId(str(like_id))}, {'$inc': {count: 1}})
    if post['keyboard_type'] == 1:
        bot.edit_message_reply_markup(inline_message_id=callback_query.inline_message_id,
                                      reply_markup=dinamic_like_keyboard_with_two_buttons(like_id))
    else:
        bot.edit_message_reply_markup(inline_message_id=callback_query.inline_message_id,
                                      reply_markup=dinamic_like_keyboard_with_three_buttons(like_id))

# to call bot.send_message with 3 different scenario, based on tagsearch result
def message_to_user_tagsearch(db_dict, message):
    tags = divide_et_impera(db_dict["linkdict"])
    # if script found tags for picture
    # send message to user with found tags, replied to query
    if tags[0] == '#':
        State.userstatus_upd_after_search(new_user.userid, tags, str(db_dict['_id']))
        bot.send_message(new_user.userid, reply_to_message_id=message, text=tags, timeout=1,
                         reply_markup=State.ok_keyb(new_user.db_id))
    else:
        # if links leads to emty pages (sometimes happens couse of SouseNao or Yandex result can be old), so no tags found
        # send error message to user
        if tags != 'likes buttons':
            bot.send_message(new_user.userid, reply_to_message_id=message, text=tags, timeout=1,
                             reply_markup=State.ok_keyb(new_user.db_id))
        # if picture need to be posted in another channel
        # send pic with inline keyb with inactive likes
        else:
            bot.send_photo(new_user.userid, photo=db_dict['photo_id'],
                           reply_markup=State.decor_like_keyb_with_2_buttons(str(db_dict["_id"])))
            db.users.update_one({}, {"$set": {str(new_user.userid): State.state1}})


# tags gathered written in another positions in db, based on user action, adds another reply keyboard,
# else same as bot_answer_based_on_searchresult
def message_to_user_for_edited_result(db_dict, tags, callback):
    if tags[0] == '#':
        bot.edit_message_text(tags, callback.from_user.id, message_id=callback.message.id,
                              parse_mode='html', reply_markup=State.use_back(new_user.db_id))
        db.user_collection.update_one({"_id": db_dict['_id']}, {"$set": {State.state_tag_dict[db_dict['state']]: tags}})
    else:
        if tags != 'likes buttons':
            bot.edit_message_text(tags, callback.from_user.id, message_id=callback.message.id,
                                  parse_mode='html', disable_web_page_preview=True,
                                  reply_markup=State.use_back(new_user.db_id))
        else:
            bot.send_photo(new_user.userid, photo=db_dict['photo_id'],
                           reply_markup=State.decor_like_keyb_with_3_buttons(str(db_dict["_id"])))
            db.users.update_one({}, {"$set": {str(new_user.userid): State.state1}})


# костыль для вызова фильтров через словарь состояний
def div_et_emp_filter(ignore):
    return None if ignore == 'nao' else ignore


# starts tagsearch with options, store it in bd, sends message to user
def perform_tagsearch_again(callback, ignore, linkdict):
    db_dict = State.get_current_post_dict(callback)
    tags = divide_et_impera(linkdict, div_et_emp_filter(ignore))
    State.save_previous_state(str(db_dict["_id"]), db_dict['userid'])
    State.set_state_dict[ignore](str(db_dict["_id"]))
    message_to_user_for_edited_result(db_dict, tags, callback)


def get_bd_id_from_callback(callback):
    return callback.data.split('_')[1]


def is_this_last_step_back_switch(db_dict, previous_state):
    if previous_state == State.state4 and 'tags2' not in db_dict.keys() or previous_state == State.state5:
        State.set_userstatus(str(db_dict['_id']), db_dict['previous state'])
        db.user_collection.update_one({"_id": ObjectId(db_dict['_id'])},
                                      {"$unset": {'previous state': ""}})
    else:
        State.set_userstatus(str(db_dict['_id']), db_dict['previous state'])
        db.user_collection.update_one({"_id": ObjectId(db_dict['_id'])},
                                      {'$set': {'previous state': previous_state}})


@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.send_message(message.from_user.id, "Hello, Im Pandora! Nice to meet you!")
    db.users.update_one({}, {"$set": {message.from_user.id: State.state1}})


@bot.message_handler(commands=['cancel'])
def cancel(message):
    bot.send_message(message.from_user.id, "Waiting for the photo")
    db.users.update_one({}, {"$set": {str(message.from_user.id): State.state1}})


@bot.message_handler(commands=['help'])
def get_help(message):
    bot.send_message(message.from_user.id, 'Here is the list of buttons and commands:'
                                           '\n"Yandex search": performs a reverse image search, using yandex engine. '
                                           'If you have a "redirecting, please wait" message from the bot - this wont help, the bot has already used this option'
                                           '\n"Ignore filters": some particular content of pictures has been blacklisted for the bot (yaoi, etc), this option helps to avoid the blacklist'
                                           '\n"Show all tags": does literally as the button says - looks for the picture on the most popular imageboards and returns the list of tags, which are found more than once'
                                           '\n"cancel": cancels all current operations, the bot awaits the next photo')


@bot.message_handler(content_types=['text'],
                     func=lambda message: db.users.find_one()[str(message.from_user.id)] == State.state9)
def userinput_edit(message):
    current_post_dict = db.user_collection.find_one({"userid": message.from_user.id, "state": State.state9})
    updated_tags = edit_input(current_post_dict['tags'], message.text)
    db.user_collection.update_one({"_id": current_post_dict['_id']}, {"$set": {'tags_edited': updated_tags}})
    State.set_usrst_tags_edited(str(current_post_dict['_id']))
    bot.send_message(current_post_dict['userid'], reply_to_message_id=current_post_dict['message_id'],
                     text=updated_tags, timeout=1, reply_markup=State.use_back(str(current_post_dict['_id'])),
                     parse_mode='html')


@bot.message_handler(content_types=['text'])
def bot_answer_on_text(message):
    if str(message.from_user.id) not in db.users.find_one().keys():
        bot.send_message(message.from_user.id, "Im working only with pictures, sorry")
        db.users.update_one({}, {"$set": {str(message.from_user.id): State.state1}})
    else:
        bot.send_message(message.from_user.id, "Im working only with pictures, sorry")


# downloads photo in temp dirrectory, compairing it with db, performs reverse search, returns reply based on result
@bot.message_handler(content_types=['photo'], func=lambda message:
                     db.users.find_one()[str(message.from_user.id)] == State.state1 and message.via_bot is None)
def bot_base_bhoto_handle(message):
    # store data in class for easier handle betwieen func s
    new_user.userid = message.from_user.id
    new_user.message_id = message.message_id
    # dowloads image
    src, downloaded_bytes = bot_downloadimage(message)
    # compairing image
    answer = bot_is_picture_already_posted(src, downloaded_bytes)
    # in case image posteted after bot released (there is another directory for bot photos)
    if answer != 'passed' and answer != 1:
        bot.send_message(new_user.userid, answer)
    else:
        State.set_precheck_userst(new_user.userid)
        # reverse search query
        check, imageboard1 = bot_call_to_reverse_searchers(src, message)
        # creates record in db
        dictlist = [message.from_user.id, message.message_id, message.photo[-1].file_id,
                    [message.photo[-1].width, message.photo[-1].height], imageboard1, src, None]
        new_user.db_id = db.user_collection.insert_one(
            dict(zip([x for x in (tempdict_sample.keys()) if x != 'status'], dictlist))).inserted_id
        # reply to image based on reverse search (try/except couse conn can be unstable)
        if check != 'source found!':
            bot.reply_to(message, check, parse_mode='html', disable_web_page_preview=True,
                         reply_markup=State.ok_keyb(new_user.db_id))
        else:
            try:
                user_db = db.user_collection.find_one({"_id": ObjectId(str(new_user.db_id))})
                bot.send_message(new_user.userid, text='source found!')
                message_to_user_tagsearch(user_db, message.message_id)
            except ConnectionError as e:
                print(e)
                bot.send_message(message.from_user.id, 'Connection Error, try again later')


@bot.callback_query_handler(lambda query: query.data.startswith('ok_') and not State.is_more_than_one_step(query)
                            and State.get_current_userstate(query.from_user.id) != State.state14)
def yes_if_first_result(callback_query):
    variables = State.get_current_post_dict(callback_query)
    if variables['state'] != State.state14:
        bot.send_photo(chat_id=variables['userid'], photo=variables['photo_id'], caption=variables['tags'],
                       reply_markup=State.decor_like_keyb_with_2_buttons(str(variables['_id'])))
        State.set_usrst_tags_conf(variables['_id'])


@bot.callback_query_handler(lambda query: query.data.startswith('ok_') and not State.is_more_than_one_step(query))
def yes_if_not_found(callback):
    db_dict = State.get_current_post_dict(callback)
    if db_dict['state'] == State.state14:
        State.set_userstatus(callback.data.split('_')[1], State.state1)


@bot.callback_query_handler(lambda query: query.data.startswith('ok_') and State.is_more_than_one_step(query))
def yes_if_more_than_one_step(callback):
    variables = State.get_current_post_dict(callback)
    bot.send_photo(chat_id=variables['userid'], photo=variables['photo_id'],
                   caption=variables[State.state_tag_dict[variables['state']]],
                   reply_markup=State.decor_like_keyb_with_2_buttons(str(variables['_id'])))
    State.set_usrst_tags_conf(variables['_id'])


# bot change reply keyboard to user can make changes
@bot.callback_query_handler(lambda query: query.data.startswith('no_'))
def no_button(callback):
    variables = State.get_current_post_dict(callback)
    if 'previous state' not in variables.keys():
        bot.edit_message_text(chat_id=callback.from_user.id, message_id=callback.message.id,
                              text=callback.message.text + State.warnstring1,
                              reply_markup=State.state_keyb_dict[variables['state']](str(variables['_id'])))

    else:
        bot.edit_message_reply_markup(chat_id=callback.from_user.id, message_id=callback.message.id,
                                      reply_markup=State.state_keyb_dict[variables['state']](str(variables['_id'])))


@bot.callback_query_handler(lambda query: query.data.startswith('nao_'))
def yandex_search(callback):
    src = State.get_current_post_dict(callback)['src']
    check, linkdict = bot_call_to_reverse_searchers(src, True)
    if check != 'source found!':
        bot.edit_message_text(check, callback.from_user.id, callback.message.id, disable_web_page_preview=True,
                              reply_markup=State.use_back(get_bd_id_from_callback(callback)))
        if not any([x for x in State.get_current_post_dict(callback)['linkdict'].keys()
                    if x != 'twitter' and x != 'deviantart' and State.get_current_post_dict(callback)['linkdict'][
                x] is not None]):
            State.set_not_found(callback.data.split('_')[1])
    else:
        perform_tagsearch_again(callback, callback.data.split('_')[0], linkdict)


@bot.callback_query_handler(lambda callback: callback.data.startswith('partial_'))
def partial_ignore_tags(callback):
    perform_tagsearch_again(callback, callback.data.split('_')[0], State.get_current_post_dict(callback)['linkdict'])


@bot.callback_query_handler(lambda query: query.data.startswith('absolute_'))
def all_tags(query):
    perform_tagsearch_again(query, query.data.split('_')[0], State.get_current_post_dict(query)['linkdict'])


@bot.callback_query_handler(lambda query: query.data.startswith('buttons_'))
def add_buttons_command(query):
    current_dict = State.get_current_post_dict(query)
    bot.send_photo(current_dict['userid'], photo=current_dict['photo_id'],
                   reply_markup=State.decor_like_keyb_with_3_buttons(get_bd_id_from_callback(query)))


@bot.callback_query_handler(lambda callback: callback.data.startswith('edit_'))
def prepare_edit(callback):
    bot.edit_message_reply_markup(callback.from_user.id, callback.message.id, reply_markup=None)
    bot.send_message(callback.from_user.id, "Now send me message, how you like to edit post:"
                                            " send me tags, which already in subscribtion, if you want to delete them,"
                                            " send me '#ecchi', '#nude' or '#sfw' if you want to edit content rate, "
                                            " send me new tags if you want to add them: capitalized words atter '#' counts as character or fandom name,"
                                            " otherwise they count as usual tags and placed at the end of the description. Also, you must type 'By:' "
                                            " before artist hashtag")
    State.s_u_tag_edit(get_bd_id_from_callback(callback))


# if no changes has been made
@bot.callback_query_handler(lambda query: query.data.startswith('back_')
                            and 'previous state' not in State.get_current_post_dict(query).keys())
def back_to_confirm(query):  # returns ok/nook keyb without back
    variables = State.get_current_post_dict(query)
    bot.edit_message_text(variables['tags'], query.from_user.id, query.message.id,
                          reply_markup=State.ok_keyb(str(variables['_id'])))


# back from filters menu
@bot.callback_query_handler(lambda query: query.data.startswith('back_')
                            and not getattr(query.message.reply_markup.keyboard[0][0], 'text') == 'OK')
def back_to_edit_choice(query):
    if not any([x for x in query.message.json['reply_markup']['inline_keyboard']
                if any([y.startswith('unfilter') for y in x[0].values()])]):
        variables = State.get_current_post_dict(query)
        bot.edit_message_reply_markup(query.from_user.id, query.message.id,
                                      reply_markup=State.state_keyb_dict[variables['state']]
                                      (State, bd_id=str(variables['_id'])))


# back from edit menu no userinput edit
@bot.callback_query_handler(lambda query: query.data.startswith('back_')
                            and not getattr(query.message.reply_markup.keyboard[0][0], 'text') == 'OK')
def back_to_intermediate_confirm(query):
    if any([x for x in query.message.json['reply_markup']['inline_keyboard']
            if any([y.startswith('unfilter') for y in x[0].values()])]):
        bot.edit_message_reply_markup(query.from_user.id, query.message.id,
                                      reply_markup=State.use_back(get_bd_id_from_callback(query)))


# back from confirm menu no edit
@bot.callback_query_handler(lambda query: query.data.startswith('back_')
                            and State.get_current_userstate(query.from_user.id) != State.state9
                            or State.get_current_userstate(query.from_user.id) != State.state10)
def back_from_intermediate_not_edited(query):
    db_dict = State.get_current_post_dict(query)
    bot.edit_message_text(db_dict[State.state_tag_dict[db_dict['previous state']]], query.from_user.id,
                          query.message.id,
                          reply_markup=State.state_keyb_dict[db_dict['state']]
                          (State, bd_id=str(db_dict['_id'])))
    one_more_step_back = State.stepback_state(query)
    is_this_last_step_back_switch(db_dict, one_more_step_back)


# back from confirm menu after userinput edit
@bot.callback_query_handler(lambda query: query.data.startswith('back_')
                            and State.get_current_userstate(query.from_user.id) == State.state9
                            or State.get_current_userstate(query.from_user.id) == State.state10)
def back_from_edit(query):
    db_dict = State.get_current_post_dict(query)
    bot.edit_message_text(db_dict[State.return_last_exist_tag_position(query)],
                          query.from_user.id,
                          query.message.id,
                          reply_markup=State.state_keyb_dict[State.stepback_state(query)]
                          (State, bd_id=str(db_dict['_id'])))
    one_more_step_back = State.stepback_state(query)
    is_this_last_step_back_switch(db_dict, one_more_step_back)


@bot.callback_query_handler(lambda query: query.data.startswith('like_') and 'dis' not in query.data)
def press_like(callback_query):
    like_counter(callback_query, "users_who_like", "like_count")


@bot.callback_query_handler(lambda query: query.data.startswith('dislike_'))
def press_dislike(callback_query):
    like_counter(callback_query, "users_who_dislike", "dislike_count")


@bot.callback_query_handler(lambda query: query.data.startswith('disgusting_'))
def press_disgusting(callback_query):
    like_counter(callback_query, "users_who_disgust", "disgusting_count")


@bot.callback_query_handler(lambda query: query.data.startswith('fine_'))
def press_fine(query):
    like_counter(query, "users_who_fine", "fine_count")

# creates new position in bd, sets inline query and answer it, sets userst to accept new post
@bot.inline_handler(lambda query: query.query is not None)
def create_post(inline_query):
    query = inline_query.query
    if len(str(query)) > 0:
        try:
            variables = db.user_collection.find_one({"_id": ObjectId(inline_query.query)})
            if variables['tags']:
                ziplist = dictsample_for_channel_post1.copy()
                ziplist.update({"id": variables['message_id'], "src": resiser(variables['src'], variables['size']),
                                "photo": variables['photo_id'], "width": variables["size"][0],
                                "height": variables['size'][1], "keyboard_type": 1})
                callback_id = db.posts.insert_one(ziplist).inserted_id
                img = types.InlineQueryResultCachedPhoto(1, photo_file_id=variables['photo_id'],
                                                         caption=variables['tags'], parse_mode='html',
                                                         reply_markup=dinamic_like_keyboard_with_two_buttons(
                                                             str(callback_id)))
            else:
                ziplist = dictsample_for_channel_post2.copy()
                ziplist.update({"id": variables['message_id'], "src": resiser(variables['src'], variables['size']),
                                "photo": variables['photo_id'], "width": variables["size"][0],
                                "height": variables['size'][1], "keyboard_type": 2})
                callback_id = db.posts.insert_one(ziplist).inserted_id
                img = types.InlineQueryResultCachedPhoto(1, photo_file_id=variables['photo_id'],
                                                         reply_markup=dinamic_like_keyboard_with_three_buttons(
                                                             str(callback_id)))
            bot.answer_inline_query(inline_query.id, results=[img])
            State.set_userst_done(inline_query.query)
        except ConnectionAbortedError as e:
            print(e)


bot.polling(none_stop=True, allowed_updates=True, interval=5)


@atexit.register
def goodbye():
    browser.quit()
