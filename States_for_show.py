from bson.objectid import ObjectId
from pymongo import MongoClient
from telebot import types

client = MongoClient('')
db = client['']


class States:
    def __init__(self, bot):
        self.bot = bot

    state1 = "waiting for post"
    state2 = "pre-check complete"
    state3 = "not found on nao"
    state4 = "waiting for tags approval, redirected"
    state5 = "waiting for tags approval, not redirected"
    state6 = 'tags approval after partial ignore'
    state7 = 'tags approval afer full ignore'
    state9 = 'waiting for user input'
    state10 = 'tags edited'
    state11 = 'tags approved'
    state12 = "posted"
    state14 = 'not found'
    statuses = [state1, state2, state3, state4, state4, state6, state7, state9, state10, state11]
    warnstring1 = '\n' + '\n### ###' + '\nLets see, what we can do' + \
                  '\n If you not familar with commands, you might looking for /help'

    def get_current_userstate(self, user_id):
        return db.users.find_one()[str(user_id)]

    def get_current_post_dict(self, callback):
        return db.user_collection.find_one({"_id": ObjectId(callback.data.split('_')[1])})

    def is_was_redirected_userstatus_upd_with_tags(self, userid, result, bd_id, state, tag_position=None):
        if not tag_position:
            db.user_collection.update_one({"_id": ObjectId(str(bd_id))},
                                          {"$set": {'tags': result, "state": state}})
            db.users.update_one({}, {"$set": {str(userid): state}})
        else:
            db.user_collection.update_one({"_id": ObjectId(str(bd_id))},
                                          {"$set": {tag_position: result, "state": state}})
            db.users.update_one({}, {"$set": {str(userid): state}})

    def userstatus_upd_after_search(self, userid, result, current_bd):
        if self.get_current_userstate(userid) != self.state3:
            db.user_collection.update_one({"_id": ObjectId(str(current_bd))},
                                          {"$set": {'tags': result, "state": self.state4}})
            db.users.update_one({}, {"$set": {str(userid): self.state4}})
        else:
            db.user_collection.update_one({"_id": ObjectId(str(current_bd))},
                                          {"$set": {'tags': result, "state": self.state5}})
            db.users.update_one({}, {"$set": {str(userid): self.state5}})

    def set_userstatus(self, bd_id, status):
        db.user_collection.update_one({"_id": ObjectId(str(bd_id))},
                                      {"$set": {'state': status}})
        userid = db.user_collection.find_one({"_id": ObjectId(str(bd_id))})['userid']
        db.users.update_one({}, {"$set": {str(userid): status}})

    def s_u_tag_edit(self, bd_id):
        self.set_userstatus(bd_id, self.state9)

    def set_precheck_userst(self, userid):
        db.users.update_one({}, {"$set": {str(userid): self.state2}})

    def userst_upd_after_yand_search(self, bd_id):
        self.set_userstatus(bd_id, self.state4)

    def userst_upd_after_nao_search(self, bd_id):
        self.set_userstatus(bd_id, self.state5)

    def userst_upd_part_ign(self, bd_id):
        self.set_userstatus(bd_id, self.state6)

    def userst_upd_full_ignore(self, bd_id):
        self.set_userstatus(bd_id, self.state7)

    def set_usrst_tags_edited(self, bd_id):
        self.set_userstatus(bd_id, self.state10)

    def set_usrst_tags_conf(self, bd_id):
        self.set_userstatus(bd_id, self.state11)

    def set_userst_done(self, bd_id):
        db.user_collection.update_one({"_id": ObjectId(str(bd_id))},
                                      {"$set": {'state': self.state12}})
        userid = db.user_collection.find_one({"_id": ObjectId(str(bd_id))})['userid']
        db.users.update_one({}, {"$set": {str(userid): self.state1}})

    def set_not_found(self, bd_id):
        self.set_userstatus(bd_id, self.state14)

    def stepback_state(self, callback):
        list_of_tags = [x for x in self.get_current_post_dict(callback).keys() if x.startswith('tags')]
        if len(list_of_tags) <= 2:
            return self.get_current_post_dict(callback)['previous state']
        elif self.get_current_post_dict(callback)['state'] == self.state9 \
                or self.get_current_post_dict(callback)['state'] == self.state10:
            return [x[0] for x in self.state_tag_dict.items() if x[1] == self.return_last_exist_tag_position(callback)][
                0]
        else:
            if 'tags2' not in list_of_tags:
                backstate = self.statuses.index(self.get_current_userstate(callback.from_user.id)) - 1
                return self.statuses[backstate] if self.statuses[backstate] != self.state5 else self.state4
            else:
                backstate = self.statuses.index(self.get_current_userstate(callback.from_user.id)) - 1
                return self.statuses[backstate]

    def save_previous_state(self, bd_id, userid):
        db.user_collection.update_one({"_id": ObjectId(str(bd_id))},
                                      {"$set": {'previous state': self.get_current_userstate(userid)}})

    # adds bd_id_to button callback data
    def buttongen(self, name, data, bd_id):
        button = types.InlineKeyboardButton(name, callback_data='{}_{}'.format(data, bd_id))
        return button

    def ok_keyb(self, bd_id):
        ok1 = self.buttongen('OK', 'ok', bd_id)
        no1 = self.buttongen('Not OK', 'no', bd_id)
        keyboard1 = types.InlineKeyboardMarkup(row_width=2)
        keyboard1.row(ok1, no1)
        return keyboard1

    # base keyboard for edit caption under picture
    # returs telebot.types.InlineKeyboardMarkup
    def edit_keyboard(self, bd_id):
        yandex = self.buttongen('Search with Yandex', 'nao', bd_id)
        filters = self.buttongen('Ignore filters', 'unfilter', bd_id)
        edit = self.buttongen('Edit tags', 'edit', bd_id)
        back = self.buttongen('Back', 'back', bd_id)
        just_likes = self.buttongen('Just_likes', 'buttons', bd_id)
        keyboard2 = types.InlineKeyboardMarkup()
        keyboard2.row(yandex), keyboard2.row(filters), keyboard2.row(edit), keyboard2.row(just_likes)
        keyboard2.row(back)
        return keyboard2

    # if user used yandex search option or it was used atomatically
    # returs telebot.types.InlineKeyboardMarkup
    def edit_no_yandex_keyboard(self, bd_id):
        filters = self.buttongen('Ignore filters', 'unfilter', bd_id)
        edit = self.buttongen('Edit tags', 'edit', bd_id)
        back = self.buttongen('Back', 'back', bd_id)
        just_likes = self.buttongen('Just_likes', 'buttons', bd_id)
        keyboard3 = types.InlineKeyboardMarkup()
        keyboard3.row(filters), keyboard3.row(edit), keyboard3.row(just_likes), keyboard3.row(back)
        return keyboard3

    # keyboard for deeper edit
    # returs telebot.types.InlineKeyboardMarkup
    def use_back(self, bd_id):
        ok1 = self.buttongen('OK', 'ok', bd_id)
        no1 = self.buttongen('Not OK', 'no', bd_id)
        back = self.buttongen('Back', 'back', bd_id)
        keyboard4 = types.InlineKeyboardMarkup(row_width=2)
        keyboard4.row(ok1, no1)
        keyboard4.row(back)
        return keyboard4

    # keyboard for edit tags with switch inner tags filters, see divide_et_impera in core for more
    # returs telebot.types.InlineKeyboardMarkup
    def filters_keyboard(self, bd_id):
        partial = self.buttongen('Ignore filters', 'partial', bd_id)
        full = self.buttongen('Almost all existing tags', 'absolute', bd_id)
        back = self.buttongen('Back', 'back', bd_id)
        filter_keyboard = types.InlineKeyboardMarkup()
        filter_keyboard.row(partial)
        filter_keyboard.row(full)
        filter_keyboard.row(back)
        return filter_keyboard

    # user used partial ignore option in filters menu
    # returs telebot.types.InlineKeyboardMarkup
    def filters_no_part_ignore_keyboard(self, bd_id):
        full = self.buttongen('Almost all existing tags', 'absolute', bd_id)
        back = self.buttongen('Back', 'back', bd_id)
        filter_keyboard = types.InlineKeyboardMarkup()
        filter_keyboard.row(full)
        filter_keyboard.row(back)
        return filter_keyboard

    # user used absolute ignore option in filters menu
    # returs telebot.types.InlineKeyboardMarkup
    def filters_keyboard_no_absolute_ignore(self, bd_id):
        partial = self.buttongen('Ignore filters', 'partial', bd_id)
        back = self.buttongen('Back', 'back', bd_id)
        filter_keyboard = types.InlineKeyboardMarkup()
        filter_keyboard.row(partial)
        filter_keyboard.row(back)
        return filter_keyboard

    # two with inactive likes to preview post
    # returs telebot.types.InlineKeyboardMarkup
    def decor_like_keyb_with_2_buttons(self, db_id):
        like = types.InlineKeyboardButton('💚', callback_data='1')
        dislike = types.InlineKeyboardButton('👎', callback_data='1')
        post = types.InlineKeyboardButton('Post', switch_inline_query=db_id)
        dec_like_keyb1 = types.InlineKeyboardMarkup(row_width=2)
        dec_like_keyb1.row(like, dislike)
        dec_like_keyb1.row(post)
        return dec_like_keyb1

    def decor_like_keyb_with_3_buttons(self, db_id):
        like2 = types.InlineKeyboardButton('💓', callback_data='1')
        disgus = types.InlineKeyboardButton('😰', callback_data='1')
        fine = types.InlineKeyboardButton('👌', callback_data='1')
        post2 = types.InlineKeyboardButton('Post', switch_inline_query=db_id)
        dec_like_keyb2 = types.InlineKeyboardMarkup()
        dec_like_keyb2.row(like2, fine, disgus)
        dec_like_keyb2.row(post2)
        return dec_like_keyb2

    # switcher between keyboard with yandex search and keyboard without it
    def keyboard_redirect_switch(self, bd_id):
        var = db.user_collection.find_one({"_id": ObjectId(bd_id)})
        if var['previous state'] == self.state4:
            return self.edit_no_yandex_keyboard(bd_id)
        elif var['previous state'] == self.state5:
            return self.edit_keyboard(bd_id)

    def return_last_exist_tag_position(self, callback):
        bd_dict = self.get_current_post_dict(callback)
        tag_positions = [x for x in self.state_tag_dict.values() if x in bd_dict.keys() and x != 'tags_edited']
        return tag_positions[-1]

    def is_more_than_one_step(self, callback):
        if 'previous state' in self.get_current_post_dict(callback).keys():
            return True
        else:
            return False

    set_state_dict = {'nao': userst_upd_after_yand_search,
                      '': userst_upd_after_nao_search,
                      'partial': userst_upd_part_ign,
                      'absolute': userst_upd_full_ignore,
                      }

    state_keyb_dict = {state4: edit_no_yandex_keyboard,
                       state5: edit_keyboard,
                       state6: filters_no_part_ignore_keyboard,
                       state7: filters_keyboard_no_absolute_ignore,
                       state10: keyboard_redirect_switch}

    # link between current state and field with tags in bd
    state_tag_dict = {state4: 'tags2',
                      state6: 'tags3',
                      state7: 'tags4',
                      state10: 'tags_edited'}
