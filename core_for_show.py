import json
import requests as req
from bs4 import BeautifulSoup as bs
from bson.objectid import ObjectId
from pixivpy3 import AppPixivAPI
from web_interface_for_show import imgbrd_dict
from pymongo import MongoClient
import re

client = ''
db = ''
nest_collection = ''


# classes of parsers for each imagepoard bot works with
# class name = imageboard name
# booru_strippers edit descriptions so as to leave only the body of the tag
# description_cleaners is loops for taglists with edits unique for each imageboard case (if nessesary)
class Boardss:

    def __init__(self, url, fandom=None, artist=None, character=None, tags=None):
        self.url = url
        self.fandom = fandom
        self.artist = artist
        self.character = character
        self.tags = tags

    def getsoup(self):
        res = self.url
        text = req.get(res).text
        soup = bs(str(text), features='lxml')
        return soup


class Booru(Boardss):

    def __init__(self, url):
        super(Booru, self).__init__(url)

    def fill_class_attributes(self):
        soup = self.getsoup()
        self.tags = [x.extract() for x in soup.findAll(attrs={'class': "tag-type-general"})]
        self.artist = [x.extract() for x in soup.findAll(attrs={'class': "tag-type-artist"})]
        self.fandom = [x.extract() for x in soup.findAll(attrs={'class': "tag-type-copyright"})]
        self.character = [x.extract() for x in soup.findAll(attrs={'class': "tag-type-character"})]

    def booru_stripper(self, string):
        separation1 = string.text
        separation2 = separation1.split(' ', maxsplit=1)
        separation3 = separation2[1].rsplit(' ', maxsplit=2)
        return separation3[0]

    def description_cleaner(self, list_a):
        for i in range(len(list_a)):
            list_a[i] = self.booru_stripper(list_a[i])


# Pixiv woks via api with json of artists ids and nicknames
class Pixiv(Boardss):

    def __init__(self, url):
        super(Pixiv, self).__init__(url)

    def id_get(self):
        list = self.url.rsplit('=')
        id = list[2]
        return id

    @staticmethod
    def pixiv_artist(dict):
        artist = []
        artist_var = dict['name']
        with open('pixiv_artists.json', 'r', encoding='utf-8') as file:
            pixiv_base = json.load(file)
        if artist_var in pixiv_base['artists']:
            artist.append(pixiv_base['artists'][artist_var])
        else:
            artist.append(artist_var)
        return artist

    def pixiv_tags(self, dict):
        tags = []
        try:
            for tag in dict:
                check = tag['translated_name']
                if check is not None:
                    tags.append(check)
            else:
                if tags:
                    tags.append('pixiv')
        except KeyError:
            return 'there is no tags'
        return tags

    def fill_class_attributes(self):
        api = AppPixivAPI()
        api.auth(refresh_token='')
        result = api.illust_detail(self.id_get())
        self.tags = self.pixiv_tags(result['illust']['tags'])
        self.artist = self.pixiv_artist(result['illust']['user'])


class Gelbooru(Booru):

    def __init__(self, url):
        super(Gelbooru, self).__init__(url)

    def booru_stripper(self, string):
        separation1 = string.text
        separation2 = separation1.split(' ', maxsplit=1)
        separation3 = separation2[1].rsplit(' ', maxsplit=1)
        return separation3[0]


class Danbooru(Booru):

    def __init__(self, url):
        super(Danbooru, self).__init__(url)

    def description_cleaner(self, list):
        for i in range(len(list)):
            list[i] = list[i]['data-tag-name'].replace('_', ' ')


class Yandere(Booru):
    def __init__(self, url):
        super(Yandere, self).__init__(url)

    def booru_stripper(self, string):
        sep1 = string.find('a').find_next('a')
        text = sep1.get_text().strip()
        return text


class Sankaku(Booru):
    def __init__(self, url):
        super(Sankaku, self).__init__(url)

    def booru_stripper(self, string):
        sep1 = string.find('a').get_text().strip()
        return sep1


class AnimePictures(Booru):
    def __init__(self, url):
        super(AnimePictures, self).__init__(url)

    def fill_class_attributes(self):
        soup = self.getsoup()
        self.tags = [x.extract() for x in soup.findAll(
            attrs={'href': True, 'artist': False, 'character': False, 'copyright': False, 'pictures': True, 'tag': True,
                   'with': True})]
        self.artist = [x.extract() for x in
                       soup.findAll(attrs={'artist': True, 'pictures': True, 'tag': True, 'with': True})]
        self.fandom = [x.extract() for x in
                       soup.findAll(attrs={'copyright': True, 'pictures': True, 'tag': True, 'with': True})]
        self.character = [x.extract() for x in
                          soup.findAll(attrs={'character': True, 'pictures': True, 'tag': True, 'with': True})]

    def description_cleaner(self, list):
        for i in range(len(list)):
            list[i] = list[i].text


class Rule34(Booru):
    def __init__(self, url):
        super(Rule34, self).__init__(url)

    def fill_class_attributes(self):
        soup = self.getsoup()
        self.tags = [x.extract() for x in soup.findAll(attrs={'class': "general-tag"})]
        self.artist = [x.extract() for x in soup.findAll(attrs={'class': "artist-tag"})]
        self.fandom = [x.extract() for x in soup.findAll(attrs={'class': "copyright-tag"})]
        self.character = [x.extract() for x in soup.findAll(attrs={'class': "character-tag"})]

    def booru_stripper(self, string):
        try:
            sep1 = string.a.get_text().strip()
            if sep1 != 'Flag for Deletion' or sep1 != '':
                return sep1
            else:
                pass
        except AttributeError:
            pass

    def description_cleaner(self, list):
        list1 = []
        for i in range(len(list)):
            tag = self.booru_stripper(list[i])
            if tag is not None and tag != '' and tag != 'Flag for Deletion':
                list1.append(tag)
        list.clear()
        list.extend(list1)


class Xbooru(Booru):
    def __init__(self, url):
        super(Xbooru, self).__init__(url)

    def booru_stripper(self, string):
        sep1 = string.a.get_text()
        return sep1


# Reactor have many garbage tags in descriptions so class have filter list of most common of them
class Reactor(Boardss):
    filter_list = ['anime', 'artist', 'фэндомы', 'Игры', '@dcm9«rousbrïde', 'art девушка', 'art (арт)',
                   ]

    def __init__(self, url, artist=None, tags=None):
        self.url = url
        self.artist = artist
        self.tags = tags

    def fill_class_attributes(self):
        soup = self.getsoup()
        self.tags = soup.find(attrs={'class': "post_description"}, text=True)

    def tag_cleaner(self, string):
        string = string.text
        round2 = string.rsplit(' :: ', maxsplit=-1)
        self.tags = list(filter(lambda x: x not in self.filter_list, round2))


class Thatpervert(Reactor):
    def __init__(self, url, artist, tags):
        super(Thatpervert, self).__init__(url, artist, tags)


class_dict = {
    'yande.re': Yandere,
    'pixiv': Pixiv,
    'gelbooru': Gelbooru,
    'danbooru': Danbooru,
    'thatpervert': Thatpervert,
    'reactor': Reactor,
    'rule34': Rule34,
    'chan.sankaku': Sankaku,
    'xbooru': Xbooru,
    'anime-pictures': AnimePictures,
}

common_dict_sample = {'character': [],
                      'artist': [],
                      'fandom': [],
                      'tags': []}


# loop calls for imageboard classes and its metods based on presence of links in dict gained from web_interface module
# return list of lists per description type position
def class_agregator(common_dict, imgbrd_dict=imgbrd_dict):
    filled = [x for x in imgbrd_dict if imgbrd_dict[x] is not None and x in class_dict.keys()]
    for name in filled:
        parser = class_dict[name](imgbrd_dict[name])
        # call to fill_attributes method trows error sometimes
        try:
            parser.fill_class_attributes()
        except KeyError:
            pass
        # not all classes needs to edit tags gained from html
        if hasattr(parser, 'tag_cleaner'):
            attr_list = [vars(parser)[x] for x in vars(parser) if x != 'url']
            for y in attr_list:
                if y:
                    parser.description_cleaner(y)
        attr_list2 = [x for x in vars(parser) if x != 'url']
        for y in attr_list2:
            if y in common_dict:
                check = vars(parser)[y]
                if check:
                    common_dict[y].append(check)


anal_carnaval = (
    '')


# for more relevant output tags sorted based of how offen them can be found in parsed lists
# in short: function takes most long taglist as sample and check presence of if contains in other taglists and dbs
def descriptions_count_and_unite(list):
    list2 = []
    startpoint = max(list, key=len)
    if len(list) <= 2:
        list2.extend(startpoint)
    # or if all list have same length
    elif len(set(map(len, list))) == 1:
        for i in range(len(list)):
            d = 0
            for tag in list[i]:
                for x in list:
                    if tag in x:
                        d += 1
            if d >= 2 and tag not in list2:
                list2.append(tag)
            elif tag in anal_carnaval and tag not in list2:
                list2.append('anal')
            elif tag in tags_db.keys() and tag not in list2:
                list2.append(tag)
    else:
        for tag in startpoint:
            c = 0
            for i in range(len(list)):
                if tag in list[i]:
                    c += 1
            if c >= 2 and tag not in list2:
                list2.append(tag)
    list.clear()
    list.extend(list2)


def descriptions_loop_to_1list_per_type(common_dict):
    for position in common_dict:
        if common_dict[position]:
            descriptions_count_and_unite(common_dict[position])


tags_db = nest_collection.find_one({"_id": ObjectId("")})

not_ok_db = nest_collection.find_one({"_id": ObjectId("")})

artists_db = nest_collection.find_one({"_id": ObjectId("")})

fandom_db = nest_collection.find_one({"_id": ObjectId("")})

exeptions = ('female only', 'female', '1girls', '2girls')

garbage = (
    'zelda (breath of the wild)', 'capcom', 'creatures (company)', 'game freak', 'original',
    'splatoon 2: octo expansion',
    'splatoon 2', 'series', 'nintendo', 'metroid fusion', 'game', 'arms', 'scorching-hot training', 'gainax', 'doujin',
    'swimsuit mooncancer', 'titan', 'doodle', 'fbi', 'yordle', 'illustration', 'doujin', "demon's whisper wraith",
    'character request',
    'character', 'kienzan', 'lapras', 'pervert', 'original 1000+ bookmarks', 'palm bracelet', 'body chain', 'girl',
    'pixiv', 'one eye covered', 'original character')


# some of dbs are reverse-sorted: tags placed in lists as values, not keys
def get_key(queue):
    for a, b in not_ok_db.items():
        if a in not_ok_db and queue in a:
            return b
        elif queue == a:
            return b
        elif len(a) == 1:
            if queue == ''.join(a):
                return b


# hidden feautere: returns warnstring as tags if in taglist have special tags
def izvrat(donot):
    pass


def carnaval(queue):
    if queue in anal_carnaval:
        return 'likes buttons'


# filter to return only tags stored in db
# used only for common tags like clothing etc
def call_to_hashtag_db(queue):
    if queue in tags_db.keys():
        return tags_db[queue]
    else:
        pass


# since not all existing tags of characters and fandoms stored in db and them can be multi-worded
# this function adds '#' to tags and turn them into acceptable by telegramm form
# plus some of personal style edits
def inner_hashtag_generator(string):
    splitted_tagstring = string.split(' ')
    number_of_words = len(splitted_tagstring)
    if number_of_words > 2:
        generator1 = "#" + '_'.join(splitted_tagstring)
        return generator1
    else:
        if "-" in string:
            generator2 = '#' + "".join(string.title()).replace('-', '_')
            return generator2
        else:
            generator3 = '#' + "".join(string.title()).replace(' ', '')
            return generator3


# character and common tags can have title(?) referenses in parentheses
# this function pre-edit tagstring: remove parenthesis and it contains moved to character or fandom lists
def hashtag_generator(string, common_dict):
    if string not in garbage:
        if '(' in string:
            tabs = string.split('(')[1].rstrip(' ').strip(')')
            leftover = string.split('(')[0].strip()
            if tabs not in common_dict['fandom'] and tabs not in common_dict['artist']:
                if tabs not in artists_db.keys() and tabs not in fandom_db.keys():
                    common_dict['fandom'].append(tabs)
                    return inner_hashtag_generator(leftover)
                elif tabs in artists_db.keys():
                    common_dict['artist'].append(tabs)
                elif tabs in fandom_db.keys():
                    common_dict['fandom'].append(tabs)
            else:
                return inner_hashtag_generator(leftover)
        else:
            return inner_hashtag_generator(string)


def check_tag_exist_in_db(list, dict, common_dict):
    result_list = []
    for item in list:
        if item in dict.keys():
            result_list.append(dict[item])
        else:
            tag = hashtag_generator(item, common_dict)
            if tag:
                result_list.append(tag)
    return result_list


# filter for unwanted content and for mark pictures to add them 3-buttons like keyboard instead of 2 buttons
# clears tagglists in case of tags from blacklist db, keep only joke description of unwanted content
def is_tag_exist_with_filter_options(call, blacklist):
    filtered_list = []
    for queue in call:
        if queue in anal_carnaval:
            filtered_list.clear()
            blacklist.append(carnaval(queue))
            continue
        elif queue in not_ok_db.keys() and get_key(queue) != 'sex':
            donot = get_key(queue)
            filtered_list.clear()
            blacklist.clear()
            blacklist.append(izvrat(donot))
            break
        else:
            x = call_to_hashtag_db(queue)
            if x is not None and x not in garbage:
                filtered_list.append(call_to_hashtag_db(queue))
    return filtered_list


# partial filter for case of mistake in tags or posting filtered content on purpouse
def ignore_filters(call, partial=None):
    resul = []
    if not partial:
        for queue in call:
            if queue in anal_carnaval:
                resul.append(inner_hashtag_generator(queue))
                continue
            elif queue in not_ok_db.keys():
                continue
            else:
                x = call_to_hashtag_db(queue)
                if x is not None and x not in garbage:
                    resul.append(x)
    else:
        for queue in call:
            if queue in anal_carnaval:
                resul.append(carnaval(queue))
                continue
            elif queue in not_ok_db.keys():
                continue
            else:
                x = call_to_hashtag_db(queue)
                if x is not None and x not in garbage:
                    resul.append(x)
    return resul


# pixiv tags coudnt be sorted while parsing, so module needs a function to
# handle case where only data from pixiv was gathered
def pixiv_descriptions_check(call, blacklist):
    filtered_list = []
    for queue in call:
        if queue in fandom_db.keys():
            filtered_list.append(fandom_db[queue])
        elif queue in anal_carnaval:
            filtered_list.clear()
            blacklist.append(carnaval(queue))
            continue
        elif queue in not_ok_db.keys() and queue != 'sex':
            donot = get_key(queue)
            filtered_list.clear()
            blacklist.clear()
            blacklist.append(izvrat(donot))
            break
        elif queue in tags_db.keys():
            filtered_list.append(call_to_hashtag_db(queue))
        else:
            filtered_list.append(inner_hashtag_generator(queue))
    return filtered_list


def sorting(x):
    if x == '#nude':
        return 0
    elif x == '#sfw':
        return 0
    else:
        return 1


# content rate coudnt be directly gainet from general tags from image boards
# and not all sites use content rates in acceptable for parsing form etc
def content_rate_generator(list):
    if '#nude' not in list:
        if '#sfw' in list:
            list1 = sorted(list, key=sorting)
            return list1
        else:
            list2 = sorted(list, key=sorting)
            list2.insert(0, '#ecchi')
            return list2
    else:
        list3 = sorted(list, key=sorting)
        return list3


# here starts block of tagstring generator function

# for re-sort done tags
div_et_emp_dict = {'characters': None,
                   'fandom_list': None,
                   'artist_name': None,
                   'resul2': None,
                   'resul': None}


# in case only pixiv data was gained
def pixiv(common_dict, div_et_emp_dict):
    div_et_emp_dict['artist_name'] = check_tag_exist_in_db(common_dict['artist'], artists_db, common_dict)
    div_et_emp_dict['resul'] = pixiv_descriptions_check([x for x in common_dict['tags'] if x not in garbage],
                                                        div_et_emp_dict['resul2'])
    common = [div_et_emp_dict['artist_name'], div_et_emp_dict['resul']]
    return common


# normal call to tag generator function with all filters
def casual(common_dict, div_et_emp_dict):
    div_et_emp_dict['characters'] = check_tag_exist_in_db(common_dict['character'], tags_db, common_dict)
    div_et_emp_dict['fandom_list'] = check_tag_exist_in_db(common_dict['fandom'], fandom_db, common_dict)
    div_et_emp_dict['artist_name'] = check_tag_exist_in_db(common_dict['artist'], artists_db, common_dict)
    div_et_emp_dict['resul'] = is_tag_exist_with_filter_options(common_dict['tags'], div_et_emp_dict['resul2'])
    common = [div_et_emp_dict['fandom_list'], div_et_emp_dict['artist_name'],
              div_et_emp_dict['resul'], div_et_emp_dict['characters']]
    return common


# call to tag generator without content filters
def partial(common_dict, div_et_emp_dict):
    div_et_emp_dict['characters'] = check_tag_exist_in_db(common_dict['character'], tags_db, common_dict)
    div_et_emp_dict['fandom_list'] = check_tag_exist_in_db(common_dict['fandom'], fandom_db, common_dict)
    div_et_emp_dict['artist_name'] = check_tag_exist_in_db(common_dict['artist'], artists_db, common_dict)
    div_et_emp_dict['resul'] = ignore_filters([x for x in common_dict['tags'] if x != 'pixiv'], partial=1)
    common = [div_et_emp_dict['fandom_list'], div_et_emp_dict['artist_name'],
              div_et_emp_dict['resul'], div_et_emp_dict['characters']]
    return common


# call to tag generator without content filters and tag counting
def absolute(common_dict, div_et_emp_dict):
    div_et_emp_dict['characters'] = check_tag_exist_in_db(common_dict['character'], tags_db, common_dict)
    div_et_emp_dict['fandom_list'] = check_tag_exist_in_db(common_dict['fandom'], fandom_db, common_dict)
    div_et_emp_dict['artist_name'] = check_tag_exist_in_db(common_dict['artist'], artists_db, common_dict)
    div_et_emp_dict['resul'] = check_tag_exist_in_db(common_dict['tags'], tags_db, common_dict)
    common = [div_et_emp_dict['fandom_list'], div_et_emp_dict['artist_name'],
              div_et_emp_dict['resul'], div_et_emp_dict['characters']]
    return common


# for all else cases
def unnesessary_case(common_dict, div_et_emp_dict):
    div_et_emp_dict['characters'] = check_tag_exist_in_db(common_dict['character'], tags_db, common_dict)
    div_et_emp_dict['fandom_list'] = check_tag_exist_in_db(common_dict['fandom'], fandom_db, common_dict)
    div_et_emp_dict['artist_name'] = check_tag_exist_in_db(common_dict['artist'], artists_db, common_dict)
    div_et_emp_dict['resul'] = ignore_filters(common_dict['tags'])
    common = [div_et_emp_dict['fandom_list'], div_et_emp_dict['artist_name'],
              div_et_emp_dict['resul'], div_et_emp_dict['characters']]
    return common


def awoid_null_in_taglist(dicti):
    for position in dicti.keys():
        dicti[position] = [x for x in set(position) if x is not None]


def is_call_to_pixiv(common_dict, div_et_emp_dict):
    if 'pixiv' in common_dict['tags']:
        return pixiv(common_dict, div_et_emp_dict)
    else:
        return casual(common_dict, div_et_emp_dict)


# tagstring generator
def string_collector(div_et_emp_dict_copy, common):
    if div_et_emp_dict_copy['resul2'] == 0 and any(common):
        awoid_null_in_taglist(div_et_emp_dict_copy)
        result1 = content_rate_generator(div_et_emp_dict_copy['resul'])
        resultstring = ' '.join(div_et_emp_dict_copy['fandom_list']) + ' ' \
                       + ' '.join(div_et_emp_dict_copy['characters']) + ' ' + ' '.join(result1) \
                       + '\nBy: ' + ' '.join(div_et_emp_dict_copy['artist_name'])
        return str(resultstring.strip())
    elif len(div_et_emp_dict_copy['resul2']) > 0:
        return "".join(div_et_emp_dict_copy['resul2'])
    else:
        return 'I could not find any tags, sorry'


ignore_func_dict = {'partial': partial,
                    'absolute': absolute}


# call to parsers, sorting gained data, return tagstring based on data gained
def divide_et_impera(imgbrd_dict=imgbrd_dict, ignore=None):
    common_dict = {x: [] for x in common_dict_sample.keys()}
    div_et_emp_dict_copy = {x: [] for x in div_et_emp_dict.keys()}
    class_agregator(common_dict, imgbrd_dict)
    descriptions_loop_to_1list_per_type(common_dict)
    if ignore is None:
        common = is_call_to_pixiv(common_dict, div_et_emp_dict_copy)
    else:
        if ignore in ignore_func_dict:
            common = ignore_func_dict[ignore](common_dict, div_et_emp_dict_copy)
        else:
            common = unnesessary_case(common_dict, div_et_emp_dict_copy)
    return string_collector(div_et_emp_dict_copy, common)


# block for edit string based on user input stars here
# placed here couse it uses same dbs

def tag_splitter(string):
    outlist = []
    artist = []
    if '\nBy:' in string:
        outlist = string.split('\nBy:')[0].split(' ')
        artist = string.split('\nBy:')[1].split(' ')
    elif 'By:' in string:
        outlist = [x for x in string.split('By:')[0].split(' ') if x != '']
        artist = [x for x in string.split('By:')[1].split(' ') if x != '']
    else:
        outlist = string.split(' ')
    return outlist, artist


def content_tag_replacer(list1, list2):
    filterlist = ['#ecchi', '#nude', '#sfw']
    if any(x for x in list2 if x in filterlist) and any(x for x in list1 if x in filterlist):
        index = list1.index([x for x in filterlist if x in list1][0])
        list1[index] = [x for x in filterlist if x in list2][0]
        return list1
    else:
        return list1


def tag_sorting(list, titled, lowercase):
    for tag in list:
        if tag[1].istitle():
            titled.append(tag)
        else:
            lowercase.append(tag)


def tag_type_sorter(list1, list2):
    if list2:
        titled = []
        lowercase = []
        if list1:
            tag_sorting(list1, titled, lowercase)
            tag_sorting(list2, titled, lowercase)
            titled.extend(lowercase)
            return titled
        else:
            tag_sorting(list2, titled, lowercase)
            titled.extend(lowercase)
            return titled
    else:
        return list1


# delete tags from original string, if they are in userstring, adds new in particular order,
# awoids special symbols in tags, just in case
def edit_input(string, userstring):
    if string:
        samplelist, sampleartist = tag_splitter(string)
        userlist, userartist = tag_splitter(userstring)
        edited_taglist = [x for x in samplelist if x not in userlist]
        edited_taglist = content_tag_replacer(edited_taglist, userlist)
        leftovers = [x for x in userlist if x not in samplelist and x not in edited_taglist and x is not None]
        extended_tags = tag_type_sorter(edited_taglist, leftovers)
        pattern = re.compile('[\^.\[$()|*+?{\'"]')
        if userartist and len(userartist) == len(sampleartist):
            edited_tags = ' '.join([x for x in extended_tags if x != '' and re.search(pattern, x) is None]) + '\nBy: ' \
                          + ' '.join([x for x in userartist if x != '' and re.search(pattern, x) is None])
            return edited_tags
        else:
            edited_tags2 = ' '.join([x for x in extended_tags if x != ''
                                     and re.search(pattern, x) is None]) + ' ' + '\nBy: ' \
                           + ' '.join([x for x in sampleartist if x != ''
                                       and re.search(pattern, x) is None and x not in userartist])
            return edited_tags2
    else:
        userlist, userartist = tag_splitter(userstring)
        extended_tags = tag_type_sorter(None, userlist)
        pattern = re.compile('[\^.\[$()|*+?{\'"]')
        edited_tags = ' '.join([x for x in extended_tags if x != '' and re.search(pattern, x) is None]) + '\nBy: ' \
                      + ' '.join([x for x in userartist if x != '' and re.search(pattern, x) is None])
        return edited_tags


if __name__ == '__main__':
    file_path = r''
    import web_interface_for_show as wb

    Web = wb.WebSearchUrl(file_path)
    Web.get_links()
    Web.check_urls()
    print(divide_et_impera())
    print(divide_et_impera(ignore='partial'))
