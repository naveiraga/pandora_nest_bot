import numpy as np
import Image
import imagehash as ihsh
import json
import datetime
from bson.objectid import ObjectId
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['BrokenNest']


def imageopen_for_first_db(image):
    img = Image.open('C:/Users/sabla/Downloads/Telegram Desktop/ChatExport_2021-07-09/{}'.format(image))
    return img


# for this db pictures was downloaded in lesser resolution with different names regarding to db json
# so it needs string edit before open filepath
def imageopen_for_scnd_db(image):
    string = image.split('.')[0] + '_thumb.' + image.split('.')[1]
    img = Image.open('C:/Users/sabla/Downloads/Telegram Desktop/ChatExport_2021-07-31/{}'.format(string))
    return img


# new db from bot dont need any string edits, but other functions already using imageopen type of func as argument
def imageopen_for_3rd_db(src):
    return Image.open(src)

# calculate differences in hash of send picture and pictures in db of same size
# returns verdict (str) if same hash not found or id of same photo (str)
def hash_compare(image, similars, data, imgopen):
    treshhold = 0
    diff_limit = int(treshhold * (20 ** 2))
    model = ihsh.average_hash(image, hash_size=20)
    for photo in similars:
        sample = ihsh.average_hash(imgopen(photo), hash_size=20)
        if np.count_nonzero(model != sample) <= diff_limit:
            for item in data['messages']:
                try:
                    if item['photo'] == photo:
                        return item['id']
                except KeyError:
                    pass
    else:
        return 'passed'


def hash_compare_for_bot_db(image, similars, imgopen):
    treshhold = 0
    diff_limit = int(treshhold * (20 ** 2))
    model = ihsh.average_hash(image, hash_size=20)
    for photo in similars:
        sample = ihsh.average_hash(imgopen(photo['src']), hash_size=20)
        if np.count_nonzero(model != sample) <= diff_limit:
            return photo['id']
    else:
        return 'passed'


def get_presence_in_dbs_werdict(image):
    with open(r'C:\Users\sabla\Downloads\Telegram Desktop\ChatExport_2021-07-09\result.json', encoding='utf-8') as fp:
        data = json.load(fp)
    with open(r'C:\Users\sabla\Downloads\Telegram Desktop\ChatExport_2021-07-31\result.json', encoding='utf-8') as fp:
        data2 = json.load(fp)
    image_model = Image.open(image)
    width, height = image_model.size
    similars = [x['photo'] for x in
                [y for y in data['messages'] if y.get('width') is not None and y.get('photo') is not None] if
                x['width'] == width and x['height'] == height]
    similars2 = [x['photo'] for x in
                 [y for y in data2['messages'] if y.get('width') is not None and y.get('photo') is not None] if
                 x['width'] == width and x['height'] == height]
    similars3 = db.posts.find({'width': width, 'height': height})
    actual1 = hash_compare(image_model, similars, data, imageopen_for_first_db)
    actual2 = hash_compare(image_model, similars2, data2, imageopen_for_scnd_db)
    actual3 = hash_compare_for_bot_db(image_model, similars3, imageopen_for_3rd_db)
    if actual1 != 'passed':
        return '/brkennest/{}'.format(actual1)
    elif actual2 != 'passed':
        return '/c/1435716086/{}'.format(actual2)
    elif actual3 != 'passed':
        return actual3
    else:
        return 'passed'

# cut down size of image in half to store it for bot db
def resiser(bytes, size):
    file = Image.open(bytes)
    new_size = tuple(int(x / 2) for x in size)
    new_file = file.resize(new_size)
    new_src = "I:/BrokenNest/{}.jpg".format(datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))
    new_file.save(new_src, 'JPEG')
    return new_src
