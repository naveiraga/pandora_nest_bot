import requests as req
import undetected_chromedriver.v2 as uc
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup as bs
from selenium.webdriver.support.expected_conditions import presence_of_all_elements_located
from selenium.webdriver.support.wait import WebDriverWait
import time


def filepather(file):
    return 'I:\\Новая папка\\{}'.format(file)


imgbrd_dict_sample = {
    'yande.re': '',
    'pixiv': '',
    'twitter': "",
    'gelbooru': '',
    'danbooru': '',
    'thatpervert': '',
    'reactor': '',
    'rule34': '',
    'chan.sankaku': '',
    'xbooru': '',
    'anime-pictures': '',
    'deviantart': '',
    'pntr': '', }

filter_dict = {
    'gelbooru': ['sexiezpic'],
    'danbooru': ['sexiezpic'],
    'thatpervert': ['%', 'new'],
    'reactor': ['%', 'new', 'tag'],
    'rule34': ['index&q'],
    'chan.sankaku': ['?tags', 'sexiezpic'],
    'xbooru': ['list&tags'],
    'pixiv': ['hentai-img', 'hentaiporns']
}
imgbrd_dict = {x: None for x in imgbrd_dict_sample.keys()}


# This is trackback funcrion for redirecting from SauseNAO to yandex and starting selenium.
# Its its separate, so it can be replaced in another module with telebot interactions
def redirect_notice():
    print('out of variants, redirecting')


class WebSearchUrl:

    def __init__(self, file_path, browser=None, soup_from_nao=None, board_dict=None, info=None):
        self.browser = browser
        if info is None:
            info = []
        if board_dict is None:
            board_dict = imgbrd_dict
        self.file_path = file_path
        self.soup_from_nao = soup_from_nao
        self.board_dict = board_dict
        self.info = info

    # code use different points in souse Nao html for switches, so the whole soup stored in class attribute
    def get_info_from_sausenao(self):
        search_url = 'http://saucenao.com/search.php'
        file = {'file': ('1.jpg', open(self.file_path, 'rb'), 'image/jpg')}
        response = req.post(search_url, files=file, allow_redirects=True)
        result = response.text
        snao = bs(result, features='lxml')
        self.soup_from_nao = snao

    def get_sausenao_table(self):
        point = self.soup_from_nao.find(class_="result hidden")
        if point:
            valid = [x.extract() for x in point.findAllPrevious(attrs={'class': 'resulttablecontent'})]
            return valid
        else:
            try:
                return [x.extract() for x in self.soup_from_nao.findAll(attrs={'class': 'resulttablecontent'})]
            except AttributeError:
                print('check image')

    # fills imageboard dict based on presence of keyword in link
    def imgboard_dict_loop_fill(self, name, text, url):
        string = str(text).lower()
        if name not in string or self.board_dict[name] is not None:
            pass
        else:
            self.board_dict[name] = url

    # bot works with vpn, located in Japan,
    # this is func to open sites with eng lang
    def dejapanizer(self, url):
        if r'/ja.' in url:
            url_a = url.replace('ja.', '')
            print(url_a)
            return url_a
        else:
            return url

    # same as imageboard dictionary agregator, but with filter for bad links and dejapanizer
    def yandex_imgboard_dict_loop_fill(self, name, text, url):
        string = str(text).lower()
        if name not in string or self.board_dict[name] is not None:
            pass
        else:
            if name in filter_dict.keys():
                if any(x for x in filter_dict[name] if x in url):
                    pass
                    print(url)
                else:
                    url1 = self.dejapanizer(str(url))
                    self.board_dict[name] = url1
            else:
                url1 = self.dejapanizer(str(url))
                self.board_dict[name] = url1

    # just loops for dictionary filling
    def fill_imageboard_dictionary(self, text, url):
        for key in self.board_dict.keys():
            self.imgboardDictionaryAgregator(key, text, url)

    def yandex_fill_dict(self, text, url):
        for key in self.board_dict.keys():
            self.yandex_imgboard_dict_loop_fill(key, text, url)

    # turns on yandex search, based on argument
    def mainual_switch(self, ignore_nao='off'):
        if ignore_nao == 'on':
            retry = self.soup_from_naonao.find(attrs={'id': 'yourimageretrylinks'}).find_all('a')
            ya = retry[5].get('href')
            return ya

    # run selenium headless window and gets beautifoul soup from it
    def yandex_soup(self, yandex_url):
        self.browser.get(yandex_url)
        WebDriverWait(self.browser, 100).until(presence_of_all_elements_located)
        yandex_page = self.browser.page_source
        soup = bs(yandex_page, features='lxml')
        less = [x.extract() for x in soup.find_all(class_='CbirSites-Item')]
        return less

    # parses yandex search page for links to sites in imageboard dict
    def get_yandex(self, less_similar_yandex):
        for test in less_similar_yandex:
            check = test.div.nextSibling.text
            href = test.div.a.get('href')
            self.yandex_fill_dict(check, href)

    # From somepoint yandex output has ceased to be non-relevant or links lead just to pictures without any html-content
    # so here is function to awoid most bad links, fills imageboard dict
    def fucking_yandex_filtering(self, less_similar_yandex):
        blacklist = ['thumbnail', 'search', 'news', 'popular']
        for url_block in less_similar_yandex:
            name = url_block.div.nextSibling.get_text()
            url1 = url_block.div.a.get('href')
            if not any(x for x in blacklist if x not in str(url1)):
                self.yandex_fill_dict(name, url1)
            else:
                url2 = url_block.div.nextSibling.a.get('href')
                self.yandex_fill_dict(name, url2)

    # SauseNao table has 2 separate cells with links per result with different position of link
    # checks for ling precense in cell
    def find_links(self, iteri):
        found1 = iteri.find(class_='resultmiscinfo').find_all('a')
        found2 = iteri.find(class_='resultcontentcolumn').find('a')
        if found1:
            for urls in found1:
                url = urls.get('href')
                self.fill_imageboard_dictionary(url, url)
        if found2:
            url = found2.get('href')
            self.fill_imageboard_dictionary(url, url)

    # if dont run selenium as function there is will executes driver every time browser called,
    # which costs a lot of time and cpu
    def bowser(self):
        options = uc.ChromeOptions()
        options.headless = True
        options.add_argument('--blink-settings=imagesEnabled=false')
        options.add_argument('--no-first-run')
        options.add_argument('--no-service-autorun')
        options.add_argument('--block-new-web-contents')
        browser = uc.Chrome(options=options)
        self.browser = browser
        browser.implicitly_wait(5)

    # filters soup from yandex search page
    def yandex_fill(self):
        if self.browser is None:
            self.bowser()
        less_similar_yandex = self.yandex_soup(self.mainual_switch(ignore_nao='on'))
        self.fucking_yandex_filtering(less_similar_yandex)

    # SauseNao result table could not contain any links per result
    def check_href(self, cell):
        found1 = cell.find(class_='resultmiscinfo').a
        found2 = cell.find(class_='resultcontentcolumn').a
        if found1 and hasattr(found1, 'href') or found2 and hasattr(found2, 'href'):
            return True
        else:
            return False

    # functions for sort sauseNao results, based on their similarity to original image
    def check_high_similar_with_link(self, iterable):
        sim_procent = iterable.find(class_="resultsimilarityinfo").text.strip('%')
        if float(sim_procent) >= 90 and self.check_href(iterable) is True:
            return True

    def check_less_similar_with_link(self, iterable):
        sim_procent = iterable.find(class_="resultsimilarityinfo").text.strip('%')
        if float(sim_procent) <= 90 and self.check_href(iterable) is True:
            return True

    # fill imageboard dict with links. If manual swich used or Noa table is emty turns on search via yandex
    def get_links(self, ignore_nao=False, notice=redirect_notice):
        self.get_info_from_sausenao()
        sousenao_table = self.get_sausenao_table()
        if len(sousenao_table) == 0 or ignore_nao is True:
            print('forced_redirect')
            self.yandex_fill()
        else:
            high_similar = list(filter(self.check_high_similar_with_link, sousenao_table))
            less_similar = list(filter(self.check_less_similar_with_link, sousenao_table))
            if len(high_similar) > 0:
                for linked in high_similar:
                    self.find_links(linked)
                    if not any(x for x in self.board_dict.values()):
                        notice()
                        self.yandex_fill()
            else:
                for linked_less_similar in less_similar:
                    self.find_links(linked_less_similar)
                    if not any(x for x in self.board_dict.values()):
                        notice()
                        self.yandex_fill()

    # reurns caption string for bot based on imageboard dict filling
    def check_urls(self):
        board_filter = ['twitter', 'deviantart']
        if any(x for x in self.board_dict.keys() if x not in board_filter and self.board_dict[x] is not None):
            for name, url in self.board_dict.items():
                if url and name not in board_filter:
                    print(name, url)
            return 'source found!'
        else:
            if all(self.board_dict[x] for x in board_filter):
                return 'I found only {} and {} links ' \
                       '\n{}' \
                       '\n{}'.format(board_filter[0], board_filter[1],
                                     self.board_dict['twitter'], self.board_dict['deviantart'])
            else:
                for x in board_filter:
                    if self.board_dict[x]:
                        return 'I found only {} link, sorry \n{}'.format(x, self.board_dict[x])
                else:
                    return 'I could not find any links, sorry'

    # function for inner track result links to be able check them manually
    def print_url_to_console(self):
        for name, url in self.board_dict.items():
            if url:
                print(name, url)


if __name__ == '__main__':
    start_time = time.time()
    filePath = r''
    WB = WebSearchUrl(filePath)
    WB.get_links()
    print(WB.check_urls())
    print("--- %s seconds ---" % (time.time() - start_time))
